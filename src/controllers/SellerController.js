const { Op } = require("sequelize");
const Seller = require("../models/Seller");

const index = async (req, res) => {
  try {
    const seller = await Seller.findAll();
    return res.status(200).json({ seller });
  } catch (err) {
    return res.status(500).json({ err });
  }
};

const show = async (req, res) => {
  const { id } = req.params;
  try {
    const seller = await Seller.findByPk(id);
    return res.status(200).json({ seller });
  } catch (err) {
    return res.status(500).json({ err });
  }
};

const create = async (req, res) => {
  try {
    const seller = await Seller.create(req.body);
    return res
      .status(201)
      .json({ message: "Usuário cadastrado com sucesso!", seller: seller });
  } catch (err) {
    res.status(500).json({ error: err });
  }
};

const update = async (req, res) => {
  const { id } = req.params;
  try {
    const [updated] = await Seller.update(req.body, { where: { id: id } });
    if (updated) {
      const seller = await Seller.findByPk(id);
      return res.status(200).send(seller);
    }
    throw new Error();
  } catch (err) {
    return res.status(500).json("Usuário não encontrado");
  }
};

const destroy = async (req, res) => {
  const { id } = req.params;
  try {
    const deleted = await Seller.destroy({ where: { id: id } });
    if (deleted) {
      return res.status(200).json("Usuário deletado com sucesso.");
    }
    throw new Error();
  } catch (err) {
    return res.status(500).json("Usuário não encontrado.");
  }
};

module.exports = {
  update,
  destroy,
  create,
  index,
  show,
};
