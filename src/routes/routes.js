const { Router } = require("express");
const router = Router();
const UserController = require("../controllers/UserController");
const SellerController = require("../controllers/SellerController");
const ProductController = require("../controllers/ProductController");

//authentication uses

//authentication routes

router.post("/User", UserController.create);
router.get("/User/:id", UserController.show);
router.get("/User", UserController.index);
router.put("/User/:id", UserController.update);
router.delete("/User/:id", UserController.destroy);

router.post("/Seller", SellerController.create);
router.get("/Seller/:id", SellerController.show);
router.get("/Seller", SellerController.index);
router.put("/Seller/:id", SellerController.update);
router.delete("/Seller/:id", SellerController.destroy);

router.post("/Product", ProductController.create);
router.get("/Product/:id", ProductController.show);
router.get("/Product", ProductController.index);
router.put("/Product/:id", ProductController.update);
router.delete("/Product/:id", ProductController.destroy);
 

module.exports = router;
